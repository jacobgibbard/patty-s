<div class="sticky contain-to-grid">
	<nav class="top-bar" data-topbar>
		<ul class="title-area">
			<!-- Title Area -->
			<li class="name">
				<a href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/logo.png" /></a>
			</li>
			<li class="toggle-topbar fa fa-bars"></li>
			<a href="tel:310-676-1312" class="small-phone"><i class="fa fa-phone"></i></a>
			<a href="/directions/" class="small-directions"><i class="fa fa-location-arrow"></i></a>
		</ul>
		<?php echo do_shortcode('[google-translator]'); ?>
		<div class="contact">
			<h3 class="top-number"><a href="tel:888-878-3592"><i class="fa fa-phone"></i> Call Us: (310) 676-1312</a></h3>
			<h3 class="top-address"><a href="https://www.google.com/maps/dir/''/patty's+income+tax/data=!4m5!4m4!1m0!1m2!1m1!1s0x80c2b44e71e48fb1:0xbbff0727ed2192e?sa=X&ved=0ahUKEwjZ2dHOldDKAhWivIMKHZygASMQ9RcIdTAL" target="_blank"><i class="fa fa-location-arrow"></i> 15665 Hawthorne Blvd, Lawndale, CA 90260</a> • Open Weekdays 9AM to 7PM</h3>
		</div>		
		<section class="top-bar-section">
			<?php joints_top_nav(); ?>
		</section>
	</nav>
</div>
