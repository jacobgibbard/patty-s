<div class="sticky show-for-medium-up contain-to-grid">
	<nav class="top-bar" data-topbar>
		<a href="<?php echo home_url(); ?>" rel="nofollow" class="name"><img src="/wp-content/uploads/logo.png" /></a>		
		<section class="top-bar-section right">
			<?php joints_top_nav(); ?>
		</section>
	</nav>
</div>

<div class="show-for-small-only">
	<nav class="tab-bar">
		<section class="middle tab-bar-section">
			<a href="<?php echo home_url(); ?>" rel="nofollow" class="name"><img src="/wp-content/uploads/logo.png" /></a>
		</section>
		<section class="left-small">
			<a href="#" class="left-off-canvas-toggle menu-icon" ><span></span></a>
		</section>
	</nav>
</div>
						
<aside class="left-off-canvas-menu show-for-small-only">
	<?php joints_off_canvas(); ?>    
</aside>
			
<a class="exit-off-canvas"></a>
