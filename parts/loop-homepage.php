<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

			<div class="post-content">						
			    <?php the_content('Read More'); ?>
			</div>
		    
		    <div class="post-image">
				<?php the_post_thumbnail('full'); ?>
			</div>
						
	</article> <!-- end article -->

	
	
<?php endwhile; endif; ?>							