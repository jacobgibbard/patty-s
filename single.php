<?php get_header(); ?>

<div class="page-image">
	<img src="/wp-content/uploads/page-bg.jpg" /> 
</div>
			
			<div id="content">

				<div id="inner-content" class="row">
			
					<div id="main" class="large-9 medium-9 columns first" role="main">
					
					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					    	<?php get_template_part( 'parts/loop', 'single' ); ?>
					    	
					    <?php endwhile; else : ?>
					
					   		<?php get_template_part( 'parts/content', 'missing' ); ?>

					    <?php endif; ?>
			
					</div> <!-- end #main -->
    
					<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>