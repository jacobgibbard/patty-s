<?php get_header(); ?>

<div class="page-image">
	<img src="/wp-content/uploads/page-bg.jpg" /> 
</div>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-9 medium-9 columns" role="main">
					
					    	<?php get_template_part( 'parts/loop', 'page' ); ?>
					    					
    				</div> <!-- end #main -->
    
				    <?php get_sidebar(); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>