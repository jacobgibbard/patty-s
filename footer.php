				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->

		<div class="row">
		 <div class="columns large-12 wow fadeIn">
		 	<?php echo do_shortcode('[show_testimonials]') ?>
		  </div>
		</div>

					<?php echo do_shortcode('[om_gmap zoom="13" lat="33.888539" lng="-118.353441" infowindow="<h5>Pattys Income Tax</h5>15665 Hawthorne Blvd<br />Lawndale, CA 90260" marker="http://www.pattysincometax.dev/wp-content/uploads/marker.png" styles="_light_monochrome"]') ?>				

					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
							<div class="large-4 medium-4 small-12 columns wow fadeInUp">
								<a href="tel:310-676-1312" class="phone"><i class="fa fa-phone"></i>
								(310) 676-1312</a>
							</div>
							<div class="large-4 medium-4 small-12 columns wow fadeInUp">
								<a href="/contact/"><i class="fa fa-envelope"></i>
								patty@pattysincometax.com</a>
							</div>
							<div class="large-4 medium-4 small-12 columns wow fadeInUp">
								<a href="https://www.google.com/maps/dir/''/patty's+income+tax/data=!4m5!4m4!1m0!1m2!1m1!1s0x80c2b44e71e48fb1:0xbbff0727ed2192e?sa=X&ved=0ahUKEwjZ2dHOldDKAhWivIMKHZygASMQ9RcIdTAL" target="_blank"><i class="fa fa-map-marker"></i>
								15665 Hawthorne Blvd.<br />Lawndale, CA 90260</a>
							</div>
							<div class="large-12 medium-12 small-12 columns socialmediafooter wow fadeInUp">
								<a href="https://www.facebook.com/pattysincometax/" target="_blank"><img src="/wp-content/uploads/fb-icon.png"></a>
								<a href="http://www.yelp.com/biz/pattys-income-tax-services-lawndale" target="_blank"><img src="/wp-content/uploads/yelp-icon.png"></a>
							</div>	
							<div class="large-12 medium-12 small-12 columns wow fadeInUp">
								<p>&copy; <a href="/">Patty's Income Tax -</a>Serving Lawndale for over 21 Years | All Rights Reserved | <a href="/sitemap.xml" target="_blank">Sitemap</a></p>
							</div>					
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->

		<?php wp_footer(); ?>
		
	</body>
</html> <!-- end page -->